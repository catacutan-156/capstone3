import AppNavBar from './components/AppNavBar';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import ErrorPage from './pages/Error';

import { BrowserRouter as Router , Routes, Route } from 'react-router-dom';


import './App.css';

function App() {
  return (
    <div>
      <Router>
        <AppNavBar />
        <Routes>
          <Route path="/" element={< Home />} />
          <Route path="/register" element={< Register />} />
          <Route path="/products" element={< Products />} />
          <Route path="/login" element={< Login />} />
          <Route path="/logout" element={< Logout />} />
          <Route path="/products/view" element={< ProductView />} />
          <Route path='*' element={<ErrorPage/>} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
